package b137.miniano.s02d1;

public class ArraysAndOperators {
    public static void main(String[] args) {
        System.out.print("Arrays and Operators\n");

        // Declare an array of integers
        int[] arrayOfNumbers = new int[5];

        System.out.println("The element at index 0 is: " + arrayOfNumbers[0]);
        System.out.println("The element at index 1 is: " + arrayOfNumbers[1]);
        System.out.println("The element at index 2 is: " + arrayOfNumbers[2]);
        System.out.println("The element at index 3 is: " + arrayOfNumbers[3]);
        System.out.println("The element at index 4 is: " + arrayOfNumbers[4]);

        System.out.println();

        // Mini-activity: Assign the following values to the elements of arrayOfNumbers.
        // 30, 11, 23, 35,0
        // Start assigning with the elemtn at index 0 and move forward respectively.

        // Manual initialization
        arrayOfNumbers[0] = 30;
        arrayOfNumbers[1] = 11;
        arrayOfNumbers[2] = 23;
        arrayOfNumbers[3] = 35;
        arrayOfNumbers[4] = 0;

        System.out.println("The element at index 0 is: " + arrayOfNumbers[0]);
        System.out.println("The element at index 1 is: " + arrayOfNumbers[1]);
        System.out.println("The element at index 2 is: " + arrayOfNumbers[2]);
        System.out.println("The element at index 3 is: " + arrayOfNumbers[3]);
        System.out.println("The element at index 4 is: " + arrayOfNumbers[4]);

        System.out.println();

        // Declare an array of names
        String[] arrayOfNames = {"Curry" , "Thompson" , "Green", "Durant", "Lillard"};

        System.out.println("#30 " + arrayOfNames[0]);

        System.out.println();

        // Declare an array of arbitary length
        int [] arrayOfArbitraryNumbers = {};
        //int [] arrayOfArbitraryNumbers;

        //This will create an error
        //arrayOfArbitraryNumbers[0] = 100;
        //ArrayList is the solution

    }
}
